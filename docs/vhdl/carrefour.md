# Lab of traffic light system design

## VHDL Cheat Sheet

[VHDL Cheat Sheet](../memo/index.md)

## Associated Gitlab repository

A gitlab repository `tp-carrefour-etudiant` is available in your gitlab space on [https://gitlab-df.imt-atlantique.fr](https://gitlab-df.imt-atlantique.fr), in the group corresponding to the course you are following.

To manipulate it (`clone`, `add`, `commit`, `push`, `pull`), please refer to the page [Git and Gitlab](../gitlab.md).

## Objective of the lab

This lab aims to introduce you to the design of digital integrated circuits. It aims to integrate a traffic light management system between a priority road and a path.

Starting from the description of the desired operation of the system, you will design an architecture on paper, describe it in VHDL (a language dedicated to digital electronics), simulate and synthesize the system, and finally test it on a reconfigurable circuit of the *Field-Programmable Gate Array* (FPGA) type.

A skeleton of the project will be provided to you. It contains VHDL source files, some of which are incomplete, and you will need to complete them.

## The traffic lights

This is a system for managing a crossroads between a road and a path using traffic lights as shown in the following figure. By default, the road is green.

![Crossroads plan](../img/carrefour/Carrefour.png)

If a vehicle is present on the path, a sensor detects its presence and the alternation of the lights is put in place:

  1. The road is green
  2. A vehicle is present on the path
  3. The road turns orange and then red after remaining a minimum time in green
  4. Both lights are red
  5. The path turns green, then orange and then red;
  6. Both lights are red;
  7. The road turns green for a minimum time, even if another vehicle is already present on the path.

At the start of the device, both lights must be red (which is also obtained by pressing the reset button). The times to be respected are as follows:

 - minimum duration where the road is green: TRV = 9s,
 - duration where the road or the path are orange: TROCO = 3s,
 - duration where the path is green: TCV = 7s,
 - duration where both lights are red: Tsecu = 2s.

For each of these timings, a countdown indicates to motorists the time remaining before the lights change from one state to another. This countdown is displayed on a 7-segment display.

### Description of the circuit architecture

The traffic light management circuit consists of two main modules: an operative unit and a control unit, which is described by a finite state automaton. These two combined modules implement the algorithm previously described. Each operation corresponds to the execution of an instruction given by the automaton to the operative part. For its part, the operative part sends state information to the control automaton to indicate the progress of the operation. The VHDL of the traffic management circuit is *incomplete*.

In addition to these two modules, a third one is responsible for communication with peripherals, namely the countdown display.

The global architecture is represented in the following figure:

![Global architecture of the system](../img/carrefour/CarrefourGlobal.png)

#### Operative Part

The operative unit here consists of time counters based on the clock frequency of the board at 100 MHz. The following figure shows its architecture.

A first counter (`SR_counterPulse`) generates a pulse (`SC_secondPulse`) every second, i.e. a signal that goes to '1' for one clock period once per second and remains at '0' otherwise. A second counter (`SR_counterSecond`) uses this pulse as well as an initial counting value (`I_maxCount`) to count the seconds. In addition to the clock `I_clk` and the reset `I_rst` (used asynchronously), an initialization signal `I_init` (used synchronously), an initialization value `I_maxCount`, and an enable signal `I_enable` are input. Since one second represents 100,000,000 clock cycles, this two-counter structure offers the possibility to easily modify the number of seconds to be counted to facilitate its simulation.

#### Control Part

The control part is responsible for controlling the counters and the smooth running of the algorithm. For the control implementation, we will use a finite state machine (FSM). There are two types of possible FSMs: Moore and Mealy.

With a Moore FSM, the outputs depend only on the state in which the system is (`SR_presentState`). The design is therefore simpler. On the other hand, the FSM always reacts to a change in an input with a one-cycle delay.

With a Mealy FSM, the outputs depend on the state in which the system is, but also on the inputs (if necessary). Such an FSM allows more flexibility and greater reactivity to the management of the outputs, but its design is a little more delicate.

Here we will focus on the Moore machine (unless explicitly requested by the teacher).

![Diagram of the Moore FSM](../img/carrefour/FSM-moore.png)

## Digital design work

### Preparatory work

!!! question
    You must complete the diagram of the Moore FSM based on the file `doc/img/FSM-moore.drawio`, which can be modified via the *Draw.io* application available online: [https://app.diagrams.net/](https://app.diagrams.net). Or that of Mealy if explicitly requested by the teacher, or if time permits.

To do this, you need to understand the interaction between control and operative parts. The states are already proposed, you have to provide the transition conditions between the states (the different conditions Ci on the diagram) and the values of the instructions to trigger for each state, based on the example provided for the state `RRCR2`.

### Work in the classroom

You will use the Vivado 2019.2 software to describe and synthesize your circuit based on the files available in your git repository `tp-carrefour-etudiant-*login*` on [https://gitlab.imt-atlantique.fr](https://gitlab.imt-atlantique.fr).


To launch vivado on a TP campux machine, open a terminal and run the following commands:

```bash

SETUP ELEC_VIVADO20192 # sets up the environment (license, PATH)

vivado
```


The launch of the tool can be quite long. During the sessions, you will complete the provided code of the following parts:

 - The counter module (`counter.vhd`), which will need to be tested with the associated test bench
 - The Moore FSM (`fsmMoore.vhd`)
 - The carrefour module, which is the assembly of the FSM and the counter module (`carrefourUnit.vhd`) which will need to be tested with the associated test bench
 - The 7-segment display management module (`decimalTo7segment.vhd`)

The constraint, test and integration files remaining are provided.

The circuit to be configured is a Xilinx Artix-7 100T FPGA (`XC7A100TCSG324-1`) which is embedded on the Digilent Nexys 4 board including, in addition to the FPGA, many peripherals.

The documentation of the board is available in your git repository (`doc/nexys4_rm.pdf`). This documentation will be useful to understand the use of the peripherals.
