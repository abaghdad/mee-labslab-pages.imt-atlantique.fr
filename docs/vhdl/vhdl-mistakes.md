# Common VHDL mistakes

## Foreword
The tool used for this lab is Vivado Design Suite, from Xilinx's Vitis platform.

- Step 1: run Vivado from a Terminal window:

```bash
SETUP MEE_VIVADO_LATEST
vivado &
```
Then, create a new project, using the **Create project** wizard. Do not add files to the project at this stage.

- Step 2: download the source file archive from Moodle and decompress it in a directory of your workspace dedicated to the lab.

## Exercise 1
This first exercise focuses on the different ways of describing a logical equation in VHDL. This will be illustrated with the description of an XOR (exclusive or) function.

Propose 3 different ways of describing an XOR (exclusive or) function in VHDL:

- Direct use of the XOR function,
- Description of the truth table of the operator in a single statement,
- Description of the truth table of the operator using a process.


!!! question "Todo list"
    How many processes are used and what are their natures? How do you differentiate them?

    1. Add the Exercise1.vhd and Test_bench1.vhd files as simulation sources.
    2. Complete and simulate the provided VHDL codes.
    3. Use the provided testbench to simulate and compare the 3 different architectures.
    4. Explain the way these 3 architectures are executed.

## Exercise 2

In this exercise, we describe the behaviour of a D flip-flop. The Reset input has priority over the clock signal.

!!! question "Todo list"

    1. Add the `Exercise2.vhd` and `Test_bench2.vhd` files as a simulation sources.
    2. Simulate the provided VHDL code.
    3. What can you observe on the waveform?
    4. How can you explain this behaviour?
    5. Change the VHDL code to make it work properly.

## Exercise 3

The provided VHDL code describes the behaviour of a 3x7 decoder (combinational operator). This operator has 3 inputs and 8 outputs: for a specific logical value at its inputs (e. g. `“010”`, the corresponding output (e. g. `O_out(2)` is activated).

!!! question "Todo list"

    1. Add the `Exercise3.vhd` and `Test_bench3.vhd` files as a simulation sources.
    2. Simulate the provided VHDL code.
    3. Does the decoder behave properly?
    4. If not, explain why and change the VHDL code to make it work properly.

## Exercise 4

The goal of the exercise is to simulate a down-counter modulo N where N is an integer parameter of the counter entity. A VHDL code is provided for the counter as well as a testbench.

!!! question "Todo list"

    1. Add `Exercise4.vhd` and `Test_bench4.vhd` files as simulation sources.
    2. Read the VHDL code and explain how the counting signal is implemented.
    3. Simulate the VHDL code and observe the input and output ports as well as the internal signals. Is the code behaviour compliant with the one expected? If not, why?
    4. Change the VHDL code to fix the issue and simulate again.

## Exercise 5

The provided VHDL code describes a combinational operator. It has three `std_logic` inputs, `I_2`, `I_1` and `I_0` and one `std_logic_vector` output `O_out`.

!!! question "Todo list"

    1. Add the `Exercise5.vhd` file as a design source and `Test_bench5.vhd` file as a simulation source.
    2. Simulate the VHDL code and observe the waveforms. Is the code behaviour compliant with the one expected?
    3. Run an RTL analysis (very first step of the synthesis process) and observe the result (schematic).
    4. What can you observe? Explain where does the problem come from and fix it.

## Exercise 6

The VHDL code provides two different descriptions of an adder: `ADDER1` adds two integer signals, `ADDER2` adds two integer values represented by `signed(7 downto 0)` signals. Both testbenches simulate the same input waveforms.

!!! question "Todo list"

    1. Add the `Exercise6_xx.vhd` files as design sources and `Test_bench6.vhd` file as a simulation source.
    2. Simulate both VHDL codes and observe the waveforms. Are they similar?
    3. Run an RTL analysis and observe the result.
    4. Are the elaboration results identical for `ADDER1` et `ADDER2`? If not, why?

## Exercise 7

The VHDL code describes a counter modulo $N$ which activates an output signal `O_Detect` when the counter state is $N-1$.

!!! question "Todo list"

    1. Add the `Exercise7.vhd` files as a design source and `Test_bench7.vhd` file as a simulation source.
    2. Simulate the VHDL code and observe the waveforms. Is the code behaviour compliant with the one expected?
    3. Run an RTL analysis and observe the result.
    4. Explain what is wrong and fix the issue.
