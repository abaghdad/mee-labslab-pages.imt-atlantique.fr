# UE Dispositifs médicaux connectés : Mini-projet d’intégration numérique d’algorithmes de traitement du signal

## VHDL Cheat Sheet

[VHDL Cheat Sheet](../memo/index.md)

## Groupe et dépôts Gitlab associés

Un groupe, regroupant l'ensemble des différents dépôts git, a été créé sur [https://gitlab-df.imt-atlantique.fr](https://gitlab-df.imt-atlantique.fr) pour chaque étudiants. Dans ce groupe on retrouve les dépôts correspondants aux projets :

- Loto
- Processeur de son, filtrage audio
- Filtrage EGC


Pour manipuler ces dépôt en local, ça se passe en ligne de commande via les commandes `git` : `clone`, `add`, `commit`, `push`, `pull`. Veuillez vous référer à la page [Git et Gitlab ](../gitlab.md).

## Objectif et règles à respecter

Cette séance de Travaux Pratiques est une initiation à la conception de circuits intégrés numériques. Elle permet de découvrir les différentes étapes de la description d’une architecture en VHDL (langage dédié à l’électronique) jusqu’au test sur circuit reconfigurable FPGA (Field-Programmable Gate Array), le tout avec un outil professionnel, Vivado du fondeur de circuits AMD (qui a racheté Xilinx pour ses techonologies FPGA en 2022). Les grandes étapes de la conception d’un circuit sur FPGA sont les suivantes :

1.  **conception de l’architecture** du circuit sous forme de blocs/modules organisés hiérarchiquement
2.  **description des modules** qui peut se faire par l’intermédiaire de langages de description matérielle (VHDL ou Verilog entre autres) ou d’outils de CAO proposant des modules déjà conçus paramétrables
3.  **simulation fonctionnelle** de chaque module
4.  **intégration** des modules pour réaliser le circuit complet
5.  **simulation** du circuit complet
6.  **synthèse** du circuit transformant la description hiérarchique en liste de composants logiques (portes élémentaires connectées)
7.  **simulation post-synthèse** pour vérifier que le modèle synthétisé est conforme au modèle initial
8.  **implantation physique** qui consiste à traduire la liste précédente en liste de composants de la technologie cible (un FPGA donné par exemple), puis à les placer sur la cible matérielle et à les router
9.  **simulation post-placement-routage** pour vérifier la conformité des performances
10. **configuration puis test du circuit**

Chacune des étapes de simulation et de test peut déclencher *un retour en arrière plus ou moins important en cas d’échec*.

En pratique, vous suivrez un flot simplifié, en bénéficiant du travail préparatoire des enseignants :

**Phase 1** Description et simulation de modules combinatoires et séquentiels simples :

- une description « à trous » en VHDL est fournie et doit être complétée sur la base de la description qui en est donnée
- chaque module VHDL est ensuite simulé et corrigé si nécessaire

**Phase 2** Synthèse, placement, routage et test du circuit complet :

- le flot de « compilation » matérielle doit être paramétré puis lancé
- le résultat de ce flot est analysé puis testé sur carte

Nous vous demandons de respecter les règles fondamentales qui sont :

- **R1** L’utilisation des outils de CAO ne doit débuter que lorsque l’architecture du circuit est finalisée, c’est-à-dire découpée en modules élémentaires (compteur, registre, décodeur, etc.) et que les interconnexions entre ces fonctions sont parfaitement définies.
- **R2** Le circuit est ensuite entièrement décrit en VHDL (pour ce TP)
- **R3** Le circuit est progressivement validé par des simulations unitaires
- **R4** Le circuit est entièrement **synchrone**. Ceci implique que :
  - **R4.1** toutes les bascules sont commandées par la même horloge CLK
  - **R4.2** la copie de l’entrée sur la sortie de la bascule est effectuée sur le front montant de l’horloge
  - **R4.3** l’entrée asynchrone de mise à 0 n’est utilisée qu’à l’initialisation du circuit et est active à un niveau qui sera le même sur tout le circuit, elle ne doit en aucun cas être utilisée pendant le fonctionnement normal du circuit

!!! warning
    Avant d’aborder ce TP, il est donc nécessaire de revoir les principales notions d’électronique numérique, en particulier les fonctions séquentielles qui doivent être entièrement synchrones dans ce TP. Sinon, pas de miracle...

## TP 1 d’initiation à la conception de circuit numérique sur FPGA : Loto

Voir le sujet du TP Loto ici : [Loto Lab](loto.md)

## TP 2 de conception d’un processeur de son

Voir le sujet du TP processeur de son filtre audio ici : [Filter Lab](filter.md)

## TP 3 Traitements de signaux ECG

Voir le sujet du TP de filtre ECG ici : [ECG Lab](ecg.md)

UE connected medical devices
