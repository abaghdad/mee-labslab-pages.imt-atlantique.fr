# Lab Session 1 - Tools and first applications

## Objectives of the lab
This is the first lab session of the Introduction to AI course. The goal is to introduce important concepts that we will use throughout the course, setup the development environment and demonstrate the application you will choose. 

## Prerequisites

The following skills are strongly recommended **before** starting the work for the lab session. Make sure of the following : 

- You have a working python installation and you know how to  install new packages. 
- You are using an IDE. We strongly recommend [VSCode](https://code.visualstudio.com/). 
- We recommend creating an environment specifically for this course using virtualenv. Install a recent python version (3.10 or higher)
- For this session we will need the following python packages : `numpy` `scipy`. Install them in the newly created virtual environment. 

!!!tip
      If you need a step by step guide on python, IDE, and environments, have a look at [this course](https://hub.imt-atlantique.fr/ueinfo-fise1a/s5/env/session2/practical/index.html).

In this course, we provide code snippets as examples, and you need to build your own code to fulfill the objectives. We can provide advice on how to do so, but coding is not an objective by itself. 



## Table of contents of the lab
1. [Key Concepts](#key-concepts)
2. [Introduction to Numpy](#introduction-to-numpy)
3. [Application](#application)

## Key concepts 


### Machine learning involves manupilating datasets as *arrays*. 

Arrays are sets of numbers (often, floating point or integers), and can be as simple as a single number (a singleton), or a vector of length $N$, or a $N\times M$ matrix, or more generally a tensor (e.g. a $N\times M\times P$ array would be called a tensor with three *modes*). 

In this course, we will use two different python packages to manipulate arrays : numpy and pytorch. We will introduce only numpy now, as pytorch is only needed to train deep learning solutions (covered in detail starting from session 4). 

You will find in section [Introduction to Numpy](#introduction-to-numpy) a very short introduction to what is needed to begin the course. Feel free to skip this part if you are already comfortable with the notions. 

!!!note 
      If you need more in depth explanations about numpy, the best place to start with is the official [numpy website](https://numpy.org), and in particular the [guide for absolute beginners](https://numpy.org/doc/stable/user/absolute_beginners.html) (highly recommended!!). 
.

### Mapping from inputs to the latent space.

In the [introductory course](lab1.md), we explained that we will often work with a case that is simply applying a function $f$ that performs a mapping between two spaces. We will use arrays to represent those spaces. 

In particular: 

- if we consider $ℝ^d$ as the input space and $ℝ^{l}$ the latent space (also called "embedding"), 
- $f$ is defined as a mapping $f: ℝ^{d} \rightarrow ℝ^{l}$, 
- a set of $N$ samples $x_i \in ℝ^{d}$ can be represented as a matrix $X \in ℝ^{N \times d}$ with $N$ rows and $d$ columns, and the corresponding samples in the latent spaces as a matrix $Y \in ℝ^{N \times l}$ obtained by applying $f$ to all $x_i$.

!!! note 
      For example, an image can be encoded as a set of pixels, there the input space is of dimension $width \times height \times 3$ (Red, Green and Blue). For a square image with $height=width=256$, the input space dimension is $196608$.

      A latent space for images using a deep learning model will typically be of dimension 512 or 1024. 

!!! note
    In practice, the software implementations that we will use can directly be called on such *sets* of inputs with one function call. This is also called *batch processing*, and is often computed in parallel or optimized. 

!!! warning
      In order to avoid confusions, let us clarify that the following terms / sentences have the same meaning regarding the latent space: 

      - Latent space, embedding space, in french "espace latent"
      - feature vectors, embeddings, samples / data in the latent space, 


### Getting intuitions on the latent space by computing pairwise distances

The latent space is supposed to be a lower dimensionnal representation of input data. The training of a deep model is supposed to forge a latent space that will easily separate input data of different nature. Therefore, an intuitive way to have a first look at samples in the latent space is to investigate the **distances** between samples. The assumption we make here is that **samples that are close to each other in the latent space are semantically similar in the input space**.

As a consequence, in this lab, the goal is to visualise all pairs of distances between samples in the latent space ; this is called a pairwise distance matrix. We will first do this with a synthetic example that you generate yourself with numpy, and next we'll do it with your chosen application. 

## Numpy Tutorial : generating synthetic data

This tutorial is in two steps : 

- To familiarize yourself with numpy functions, we give below a list of concepts needed and functions that can be used. Check the numpy documentation using your IDE or online to have a look at detailed parameters and outputs for those functions,
- Once you are ready, your task is to generate synthetic data, visualize it, and compute a pairwise distance matrix (details below).


### Numpy concepts and functions 

#### Creating arrays
- **Goal**: Get familiar with NumPy and create basic arrays.
  - **Functions to use**: 
    - Creating arrays with `np.array()`, `np.zeros()`, `np.ones()`, `np.arange()`.
    - Use the method `.shape` or the `np.shape()` function on any numpy array to know its shape

The `np.array()` function converts a Python list (or a list of lists) into a NumPy array. You can also create arrays filled with zeros or ones using `np.zeros()` and `np.ones()`. 

```python
import numpy as np

# Creating a 1D array from a list
array_1d = np.array([1, 2, 3, 4, 5])
print("1D Array:", array_1d)

# Creating a 2D array (list of lists)
array_2d = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
print("2D Array:\n", array_2d)

# Creating a 1D array of zeros
zeros_1d = np.zeros(5)
print("1D Array of zeros:", zeros_1d)

# Creating a 2D array of zeros
zeros_2d = np.zeros((3, 4))  # 3 rows, 4 columns
print("2D Array of zeros:\n", zeros_2d)

# Creating a 2D array of ones
ones_2d = np.ones((2, 3))  # 2 rows, 3 columns (a 2 by 3 matrix)
print("2D Array of ones:\n", ones_2d)

# Creating a 3D array of ones
ones_2d = np.ones((4, 2, 3))  # a 4 by 2 by 3 tensor
print("2D Array of ones:\n", ones_2d)

print(ones_2d.shape)
```

#### Generating random arrays
- **Goal**: Generate arrays of any by sampling random distributions.
  - **Functions to use**: 
    - Creating arrays with `np.random.normal()`, `np.random.uniform()`, `np.random.randint()`


#### Mathematical Operations with NumPy
- **Goal**: Perform basic operations on arrays.
  - **Concepts**:
    - Addition, multiplication, `np.sum()`, `np.mean()`.
    - Check the numpy tutorial [here](https://numpy.org/doc/stable/user/absolute_beginners.html#basic-array-operations) about basic array operations.
    - Note that arrays it's possible to do element-wise operations for arrays that have the same shape. Check also the concept of [broadcasting](https://numpy.org/doc/stable/user/absolute_beginners.html#broadcasting)

#### Array Manipulation and Indexing
- **Goal**: Learn the basics of array manipulation and indexing.
  - **Concepts**:
    - Simple indexing and slicing (`array[0]`, `array[1:3]`).
    - Boolean masking (`array[array > 5]`).

!!!note 
      Indexing will not be needed for Lab Session 1, but we will heavily use it from Session 2.

#### Saving and loading arrays
- **Goal**: Save and load arrays on disk.
  - **Concepts**:
    - `np.save()`,`np.savez()`, `np.savez_compressed()` and `np.load()`.

We recommend saving with `np.savez_compressed()`, so that you can put multiple arrays in the same file, with added compression to save space. 

### Generate synthetic data, visualize it and compute the pairwise distance matrix

As an application exercice for numpy, generate synthetic data with the following specifications : 

- The goal is to generate two clouds of points with different centers, in dimension 2. 
- Each cloud has 100 points
- Visualize the two cloud points using the `scatter` function from matplotlib
- Compute the pairwise distance matrix using the euclidean distance, using the `pdist` and `squareform` functions from scipy. 
- Visualize the distance matrix using the `matshow` function from matplotlib pyplot. 

Once you have plot the matrix, interpret it and call the teacher to talk about it! 

## Application

You have to choose one of the three proposed applications:

- [Computer Vision](cv.md) : biodiversity images from the iNaturalist project,
- [Audio](audio.md) : soundscapes from the ESC50 dataset,
- [Text](text.md) : based on Wikipedia. 

For each application, we provided a small sample dataset (specific for session 1). 

### Task for session 1 
We have already mapped those samples to a latent space. Your task for Session 1 is to compute and visualize a **distance matrix** between all pairs of samples in the latent space, using the euclidean distance. Exactly like you have done for the synthetic problem above : 

1. Compute the distance matrix
2. Visualize the distance matrix, for example using `matplotlib.pyplot.matshow`
3. Use the information provided in the application specific pages (links above) to interpret the result