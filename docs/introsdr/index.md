# Introduction to Software Defined Radio (SDR)

---

Author : Thierry LE GALL, R&D Engineer  
IMT Atlantique / Mathematical & Electrical Engineering Dpt.

---

!!! warning
    Under construction not yet in production for teaching (draft)

This course is an introduction to *Software Defined Radio* (SDR). SDR technology is in the heart of modern communication systems. Therefore, nowledge about SDR technology is part of the expected skills in *Communications Systems Engineering* (CSE).

## What is *Software Defined Radio* ? 

### Definition

The Institute of Electrical and Electronics Engineers (IEEE) organization defines the SDR technology as follow :

!!! question "Definition (IEEE) : Software Defined Radio"

    A type of radio in which some or all of the physical layer functions are software defined.
    
    See also : waveform processing. Contrast : hardware radio. 

### System Overview

A conventional SDR communication system can be seen as in the figure below. The transmitter (TX) and the receiver (RX) have four parts in order to process waveforms :

- Digital Base Band (DBB)
- Analogical Base Band (ABB)
- Radio Frequency (RF)
- Antennas

![Conventional SDR Communication System](../img/introsdr/conv_sdr_com_sys.png)

The DBB is to process waveforms digitally around the null-frequency ($f_0=0$) whilst the 

### Transmitter

### Receiver