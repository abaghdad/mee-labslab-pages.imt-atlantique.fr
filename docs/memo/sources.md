# Sources

- XST User Guide (chapter 7 : HDL coding techniques) [https://www.xilinx.com/support/documentation/sw_manuals/xilinx14_7/xst_v6s6.pdf](https://www.xilinx.com/support/documentation/sw_manuals/xilinx14_7/xst_v6s6.pdf)

- 65415 - Vivado Synthesis - VHDL Assertion Support [https://support.xilinx.com/s/article/65415?language=en_US](https://support.xilinx.com/s/article/65415?language=en_US)

- Get your Priorities Right — Make your Design Up to 50% Smaller [https://www.xilinx.com/support/documentation/white_papers/wp275.pdf](https://www.xilinx.com/support/documentation/white_papers/wp275.pdf)

- Vivado Design Suite User Guide (page 205) [https://www.xilinx.com/support/documentation/sw_manuals/xilinx2018_3/ug901-vivado-synthesis.pdf](https://www.xilinx.com/support/documentation/sw_manuals/xilinx2018_3/ug901-vivado-synthesis.pdf)

- Syntaxe VHDL [http://amouf.chez.com/syntaxe.htm](http://amouf.chez.com/syntaxe.htm)

- Doulos, numeric_std [https://www.doulos.com/knowhow/vhdl_designers_guide/numeric_std/](https://www.doulos.com/knowhow/vhdl_designers_guide/numeric_std/)

- GNU Emacs Reference Card [https://www.gnu.org/software/emacs/refcards/pdf/refcard.pdf](https://www.gnu.org/software/emacs/refcards/pdf/refcard.pdf)

- GNU Emacs Manual [https://www.gnu.org/software/emacs/manual/pdf/emacs.pdf](https://www.gnu.org/software/emacs/manual/pdf/emacs.pdf)

- Compact Summary of VHDL [https://www.csee.umbc.edu/portal/help/VHDL/summary.html](https://www.csee.umbc.edu/portal/help/VHDL/summary.html)

- The Golden Rules of Debugging [https://www.doulos.com/knowhow/fpga/the-golden-rules-of-debugging/](https://www.doulos.com/knowhow/fpga/the-golden-rules-of-debugging/)
