# Utilisation de Git/Gitlab

Git :

 - Site officiel : [https://git-scm.com/](https://git-scm.com/)
 - Page Wikipédia : [https://fr.wikipedia.org/wiki/Git](https://fr.wikipedia.org/wiki/Git)

Gitlab :

 - Site officiel : [https://gitlab.com/gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab)
 - Instance installée à IMT Atlantique pour usage général : [https://gitlab.imt-atlantique.fr/](https://gitlab.imt-atlantique.fr/tp-vhdl/tp-carrefour-etudiant)
 - Instance installée à IMT Atlantique dédiée aux enseignements : [https://gitlab-df.imt-atlantique.fr/](https://gitlab-df.imt-atlantique.fr/)

!!! quote "Page Wikipedia de Git"
    Git est un logiciel de gestion de versions décentralisé. C'est un logiciel libre et gratuit, créé en 2005 par Linus Torvalds, auteur du noyau Linux, et distribué selon les termes de la licence publique générale GNU version 2. Le principal contributeur actuel de Git, et ce depuis plus de 16 ans, est Junio C Hamano.
    Depuis les années 2010, il s’agit du logiciel de gestion de versions le plus populaire dans le développement logiciel et web, qui est utilisé par des dizaines de millions de personnes, sur tous les environnements (Windows, Mac, Linux)3. Git est aussi le système à la base du célèbre site web GitHub, le plus important hébergeur de code informatique.



!!! warning
    L'instance dédiée aux enseignements [https://gitlab-df.imt-atlantique.fr/](https://gitlab-df.imt-atlantique.fr/) est purgée tous les ans. Tous les groupes et projet sur supprimé en fin d'année scolaire. Pour des projets pérennes, il faut utiliser l'instance générale.

## Objectif

L'objectif d'utiliser git et gitlab lors des enseignements VHDL est de vous familiariser avec ces outils incontournables dans le domaine du développement, quelque soit le langage.

Ici, nous allons prendre pour exemple le projet [Carrefour](vhdl/carrefour.md), avec l'URL [https://gitlab-df.imt-atlantique.fr/tp-vhdl/tp-carrefour-etudiant.git](https://gitlab-df.imt-atlantique.fr/tp-vhdl/tp-carrefour-etudiant.git).

!!! warning
    Veuillez adapter l'URL pour qu'elle corresponde au TP que vous êtes en train de suivre. Les commandes et le fonctionnement de git/gitlab seront les mêmes, quelque soit le projet.

## Principes de git

Git permet de versionner un projet de développement, de manière fine et décentralisée. On garde ainsi l'historique de toutes les modifications et surtout, on peut revenir à une version précise très facilement.

Lorsque l'on fait des modifications dans un projet, on peut ajouter ces modifications à l'index de `git` grâce à la commande `git add`. Lorsque toutes les modifications souhaitées ont été indexées, on peut les valider avec la commande `git commit`, qui créé une nouvelle entrée dans l'historique du projet. Si besoin, on peut se balader entre les commit.

Si on souhaite tester le développement d'une fonctionnalité que l'on souhaite tester, sans impacter le développement principal, git permet la création de branches (`git branch`), que l'on peut ensuite fusionner (`git merge`) avec n'importe quelle autre branche, dont la principale (souvent appelé `main`).

Gitlab est une plateforme web de gestion de projet git. Ce type de plateforme facilite le travail collaboratif en donnant un point d'entrée unique pour un projet. On peut récupérer un projet (`git clone`), le modifier (`git add/commit`), et pousser ses modification local sur gitlab (`git push`), et récupérer les modifications d'autrui entre temps (`git pull/fetch`).


## Récupération d'un projet

Ouvrez un terminal (++ctrl+alt+t++) et créez/déplacez vous dans votre répertoire de travail souhaité avec les commandes
```bash
mkdir -p ~/chemin/du/repertoire/de/travail
```

L'option `-p` permet de créer les répertoire intermédiaires automatiquement sans avoir à le faire à la main pour chaque. Ensuite

```bash
cd !$
```

Bien évidement à adapter en fonction de vos besoin.

!!! tip
    Choisissez une arborescence lisible. Un bon reflexe est de segmenter par année, par enseignement et par TP. Par exemple pour le TP loto de l'enseignement MEDCON en 2A : `mkdir -p ~/2A/MEDCON/loto`

!!! warning
    Ne mettez **jamais** d'espaces d'accent ou de caractères spéciaux dans vos noms de répertoires ou de fichiers.

Ensuite, il faut *cloner* le dépôt git présent sur gitlab en local sur votre poste de travail avec la commande... `git clone` :

```bash
git clone https://gitlab-df.imt-atlantique.fr/tp-vhdl/tp-carrefour-etudiant.git
```

Il vous faudra renseigner vos login et mot de passe. Rien ne s'affiche quand vous tapez votre mot de passe, c'est normal, c'est une mesure de sécurité.

Un répertoire `tp-carrefour-etudiant` est normalement crée, et contient tous les fichiers du dépôt nécessaire au déroulement du TP. C'est une copie conforme, avec tout son historique, du dépôt distant.

Vous pouvez vérifier avec la commande `ls -alsh tp-carrefour-etudiant`

!!! note
    Il est possible d'utiliser une paire de clés SSH pour éviter d'avoir à entrer son mot de passe à chaque fois. Il faut pour cela créer une paire de clés, renseigner la clé public sur gitlab, et ensuite cloner le dépôt avec le lien adapté (celui en `git@gitlab...` et non celui en `https://gitlab....`)

## Vérifier l'état de votre dépôt local

Il est parfois utile et intéressant de vérifier l'historique de votre dépôt avec la commandes `git log` :

```bash
git log
git log --graph
```

## Configuration locale

Il est nécessaire de renseigner votre nom et votre email. Toute validation par git utilise ces informations.

```bash
git config --global user.name "votre login"
git config --global user.email votre.email@imt-atlantique.net
```

## Envoi de vos modification sur le dépôt Gitlab.

Pour envoyer des modifications sur un dépôt distant, il faut passer par plusieurs étapes.

### Ajouter les modifications à l'index local

Par exemple, vous avez modifier les fichier `counter.vhd` et `fsmMoore.vhd` (dans le répertoire `src`) et vous souhaitez prendre en compte ces modifications. Il faut d'abord se déplacer dans le répertoire du dépôt local :
```bash
cd ~/chemin/du/repertoire/de/travail/tp-carrefour-etudiant
```
Il est souvent intéressant d'avoir un aperçu des modifications effectuées dans un projet avant de faire des commandes de `add`, `commit` etc :

```bash
git status
```

puis utiliser la commande `git add` :

```bash
git add src/counter.vhd src/fsmMoore.vhd
```

!!! warning
    n'ajoutez à vos modifications que les codes sources VHDL et les éventuelles comptes rendus et schémas drawio et leur export `.png`. **Évitez d'ajouter le projet Vivado**, ou alors uniquement les scripts qui permettent de le créer/gérer.


### Valider toutes les modifications de l'index

Pour *valider* toutes les modifications ajoutées à l'index, il faut utiliser la commande `git commit`, en n'oubliant pas d'ajouter un message décrivant la nature des modifications que vous avez apporté avec l'option `-m`

Par exemple, toujours en étant dans le répertoire du projet Carrefour :

```bash
git commit -m "Modification de FSM et du counter validés par simulation dans Vivado"
```
### Envoyer les modification validées par le commit sur le dépôt distant

Pour *pousser* les modifications sur le dépôt distant (`origin`, sur la branche `main`), on utilise la commande `git push` :

```bash
git push origin main
```

La branche `main` est la branche par défaut créé lors de la création d'un projet. Il est possible de créer autant de branches que l'on souhaite, mais ce n'est pas le sujet ici.

Vous devrez renseigner vos login et mot de passe

### Récupérer des modifications distantes.

Lorsque que l'on travail à plusieurs sur un projet, on est amené à devoir récupérer (*tirer*) en local des modifications poussées entre temps sur le dépôt distant. Il est conseillé de d'abord faire `add` et `commit` avant. On utilise la commande :
```bash
git pull
```

Vous devrez renseigner vos login et mot de passe

### Récupérer de modifications distants depuis un autre dépôt distant

Ici on va prendre le cas du dépôt de référence (sur gitlab.imt-atlantique.fr) qui permet de déployer les dépôts pour les étudiants.

Ajout du dépôt distant (remote) de référence :

```bash
git remote add reference https://gitlab.imt-atlantique.fr/tp-vhdl/tp-carrefour-etudiant
```

Affichage des dépôts distants avec leurs URLs :

```bash
git remote -v
```

Récupération des modification distantes du dépôt de référence :

```bash
git fetch reference
```

Fusion des modifications de la branche main du dépôt de référence avec le dépôt local

```bash
git merge reference/main
```
