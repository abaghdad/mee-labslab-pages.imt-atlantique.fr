# SEIML HW2: Peripherals and Interrupts

## Objective of the lab

The objective of this lab is to illustrate the integration of standard peripherals and the implementation of interruptions in an embedded system. The aim is to extend the system developed in the previous lab to implement the system illustrated in Figure 1 below.

![System to be implemented in HW TP2](../img/seiml/seiml_hw2/seiml-hw2-0.png)

## A. Design and configuration of the hardware architecture, including interrupts

1. Open a terminal and setup the Vivado-Vitis environment through the command:
```bash
SETUP MEE_VITIS20202
```
2. Open Vivado:
```bash
vivado &
```
3. Open the project of the previous lab: `HW_TP1`
4. You will now create a copy of this project into `HW_TP2` to continue from the work done during the previous lab session. You can do this by selecting `File > Project > Save As…` and giving the name `HW_TP2` as show below.
![Create a copy of HW_TP1 into HW_TP2](../img/seiml/seiml_hw2/seiml-hw2-1.png)
This will duplicate the project in a separated folder.
5. Open the block design of the project by expanding `system_wrapper` in `Sources` panel, and double-click on the `system.bd` (`system_i`) file to invoke IP Integrator.
6. Configuration of the Processing System (PS): double click on the Zynq block in the block diagram of the system to open the Zynq configuration window. Enable Timer 0 of the PS as shown in Figure 1 (`MIO Configuration tab > Application Processor Unit`).
This PS-Timer 0 will not be used in this lab, but will be needed later on, in the software labs using Linux.
7. Configuration of the Programmable Logic (PL):
    - Follow the same approach that has been detailed in the previous lab to add the required peripherals (`AXI GPIO` and `AXI Timer`) as show in Figure 1.
    - Rename and configure the added standards peripherals appropriately.
    - Connect them to the rest of the system (refer to `HW_TP1` for the flow)
    - Enable interrupts for the buttons and the switches. Interrupt of axi_timer is enabled by default.
    - Connect these three interrupt sources to the Cortex-A9 processor unit through its input interrupt port IRQ_F2P. To do so, we need first to add a concat block. The concat block merges the three interrupt signals into a 3-bit vector. Note that you can still connect it to `IRQ_F2P[0:0]`. When you run the synthesis, Vivado will update it to `IRQ_F2P[0:2]`
        - Add the concat block (Add IP menu, as for `AXI GPIO` and `Timer`)
        - Select and double-click it to customize the number of input ports to 3.
        - Connect the interrupt outputs of axi_timer, buttons, and switches to the inputs of the concat block.
        - Connect the output of the concat block to `IRQ_F2P[]`. For that, you need to enable interrupts in the Cortex-A9 Zynq PS as follows:
    - You can click the regenerate button to redraw the diagram.
    - Verify the memory map in Address Editor tab, i.e. all peripherals have been assigned address spaces and there are no unmapped blocks.
![Block design of the system](../img/seiml/seiml_hw2/seiml-hw2-2.png)

## B. Make GPIO Peripheral Connections External
1. If you have not used Run Connection Automation designer assistance, you can use the same flow as in `HW_TP1` to make external the GPIO port of the buttons peripheral as illustrated in Figure 1.
2. Select the created external port and change its name to `btns_5bits`.
3. Run Design Validation (`Tools -> Validate Design`) and verify there are no errors. The design should now look similar to the diagram below.
![Block design of the system](../img/seiml/seiml_hw2/seiml-hw2-3.png)
4. **IMPORTANT**: is appears that after a “`Project Save as`”, the top-level `system_wrapper.vhd` is no more updated automatically when changes are made to in the Block design (a bug in 2020.2 version). This is an issue when the changes are related to external ports which is the case here!
So, you need to remove the `system_wrapper.vhd` from the project (confirm deleting it from disk) and create a new wrapper by right-clicking on `system.bd`, and select `Create HDL Wrapper…` Leave the `Let Vivado manage wrapper and auto-update` option selected.
5. In the Flow Navigator, click `Run Synthesis`. (Click `Save` if prompted).

## C. Generate Bitstream and Export to Vitis
1. Generate the Bitstream.
2. Export the hardware (`File > Export > Export Hardware`), include the bitstream and choose a name for the Xilinx Shell Architecture (XSA) file, like `hwtp2_wrapper`.
3. Start Vitis by clicking `Tools > Launch Vitis IDE`. **IMPORTANT**: note that when you copied the project from `HW_TP1` through “save as”, only the hardware design has been copied. Therefore, you can create at this step a new workspace for Vitis related to HW_TP2 as follows:
![Create a new workspace for Vitis related to HW_TP2](../img/seiml/seiml_hw2/seiml-hw2-4.png)

## D. Software application development in Vitis
1. Create a new Application Project to test the embedded system peripherals. You have to follow the same steps as in the first lab (create a new platform based on the generated XSA file, give a name to the application project as `hwtp2_app`, select an `Empty Application` for the template).
2. An application source code is provided in Moodle for this second lab, download it, and import it in the `src` folder.
3. In a first step, test the UART, push-buttons, switches and LEDs without using the interrupts, e.g.:
    - Display the status of the switches on the LEDs and on the UART terminal
    - Increment/decrement a global variable through push-button commands and display its value on the UART.
    Use the software drivers (functions) provided by Xilinx to interface with the instantiated peripherals.
4. Do the same tasks with activating the interrupts for the switches and the push-buttons. You need for that to uncomment the call to the function `SetupInterruptSystem()` and to complete the missing code inside this function and in the corresponding interrupt handlers.
5. Add a functionality implementing the AXI Timer device and the management of its interrupt.
    For instance sending different values on the leds every second, different led display configurations controlled by the push-buttons, etc.

To be continued... !
